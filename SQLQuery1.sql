create table tblUsers(
iID int identity(1,1) primary key,
sUsername varchar(50) not null unique,
sPassword varchar(16) not null,
sStudentID varchar(20) not null unique
)

create table tblSubjects(
iID int identity(1,1) primary key,
sName nvarchar(50) not null,
iCredit int not null
)

create table tblMarks(
iID int identity(1,1) primary key,
iUserID int not null,
iSubjectID int not null,
rFirstMark real not null,
rSecondMark real not null,
rThirdMark real not null,
rSubMark real not null,
sSubMarkByAlpha varchar(2) not null,
foreign key (iUserID) references tblUsers(iID),
foreign key (iSubjectID) references tblSubjects(iID)
)

alter proc spLoginCheck @username varchar(50), @password varchar(16)
as
select iID, sUsername, sPassword from tblUsers where sUsername=@username and sPassword=@password
exec spLoginCheck ngoc,ngoc

create proc spSignup @username varchar(50), @password varchar(16), @studentID varchar(20)
as
insert into tblUsers (sUsername,sPassword,sStudentID)
values (@username,@password,@studentID)

create proc spCheckExistUser @username varchar(50)
as
select sUsername from tblUsers where sUsername=@username

create proc spCheckExistStudent @studentid varchar(20)
as
select sStudentID from tblUsers where sStudentID=@studentid

alter proc spReturnMark @userid int
as
select tblMarks.iID,tblMarks.iSubjectID,sName,rFirstMark,rSecondMark,rThirdMark,rSubMark,sSubMarkByAlpha,bPassStatus from tblSubjects,tblMarks where @userid = iUserID and tblMarks.iSubjectID = tblSubjects.iID

exec spReturnMark 1

alter proc spReturnSubjectName
as
select iID,sName from tblSubjects order by sName asc

exec spReturnSubjectName

alter proc spAddMark @subjectID int, @userID int, @firstmark real, @secondmark real, @thirdmark real, @submark real, @submarkbyalpha varchar(2), @pass bit
as
insert into tblMarks(iUserID, iSubjectID, rFirstMark, rSecondMark, rThirdMark, rSubMark, sSubMarkByAlpha, bPassStatus)
values(@userID, @subjectID, @firstmark, @secondmark, @thirdmark, @submark, @submarkbyalpha, @pass)

alter proc spReturnMarkForUpdate @markid int
as
select sName from tblSubjects,tblMarks where @markid = tblMarks.iID and tblMarks.iSubjectID = tblSubjects.iID

exec spReturnMarkForUpdate 7

create proc spUpdateMark @markid int, @firstmark real, @secondmark real, @thirdmark real, @submark real, @submarkbyalpha varchar(2)
as
update tblMarks
set rFirstMark = @firstmark, rSecondMark = @secondmark, rThirdMark = @thirdmark, rSubMark = @submark, sSubMarkByAlpha = @submarkbyalpha
where iID = @markid

exec spUpdateMark 7,8,8,8,8,A

create proc spDeleteMark @markid int
as
delete from tblMarks where iID = @markid

alter proc spCheckForPWReset @username varchar(50),@studentid varchar(20)
as
select sUsername, sStudentID from tblUsers where sUsername = @username and sStudentID = @studentid

create proc spChangePW  (@username varchar(50),@newPW varchar (16) )
as
update tblUsers
set sPassword = @newPW where @username = sUsername

delete from tblUsers 
where iID = 2

select * from tblUsers
select  * from tblSubjects


alter table tblMarks
add bPassStatus bit not null


create proc spChangeUserPassword @iID int,@sPassword varchar(16)
as
update tblUsers set sPassword=@sPassword

create proc spCheckPassword @iID int,@sPassword varchar(16)
as
select sPassword from tblUsers 
where @iID=iID and @sPassword=sPassword

create proc spCheckStudentID @sStudentID varchar(20)
as
select sStudentID from tblUsers 
where sStudentID=@sStudentID

create proc spChangeStudentID @iID int,@sStudentID varchar(16)
as
update tblUsers set sStudentID=@sStudentID
where iID=@iID

select * from tblUsers 