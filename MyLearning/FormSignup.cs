﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyLearning
{
    public partial class FormSignup : Form
    {
        string connectionString = ConfigurationManager.ConnectionStrings["Test"].ConnectionString;
        public FormSignup()
        {
            InitializeComponent();
        }

        private void btnSignup_Click(object sender, EventArgs e)
        {
            if (txtbUsername.Text == "" || txtbPassword.Text == "" || txtbPasswordConfirm.Text == "" || txtbStudentID.Text == "")
                MessageBox.Show("Yêu cầu nhập đủ thông tin");
            else
            {
                if (txtbPassword.Text != txtbPasswordConfirm.Text)
                    MessageBox.Show("Mật khẩu nhập lại phải trùng khớp với mật khẩu đã đăng kí");
                else
                {
                    var connection = new SqlConnection(connectionString);
                    SqlCommand checkExistUser = new SqlCommand("spCheckExistUser", connection);
                    checkExistUser.CommandType = CommandType.StoredProcedure;
                    SqlParameter checkUsername = checkExistUser.Parameters.Add("@username", SqlDbType.VarChar);
                    checkUsername.Value = txtbUsername.Text;
                    connection.Open();
                    var dataReader = checkExistUser.ExecuteReader(CommandBehavior.CloseConnection);
                    if (dataReader.HasRows)
                    {
                        MessageBox.Show("Tên đăng nhập này đã có người đăng ký");
                    }
                    else
                    {
                        SqlCommand checkExistStudent = new SqlCommand("spCheckExistStudent", connection);
                        checkExistStudent.CommandType = CommandType.StoredProcedure;
                        SqlParameter checkStudentID = checkExistStudent.Parameters.Add("@studentID", SqlDbType.VarChar);
                        checkStudentID.Value = txtbStudentID.Text;
                        var dataReader1 = checkExistStudent.ExecuteReader(CommandBehavior.CloseConnection);
                        if (dataReader1.HasRows)
                        {
                            MessageBox.Show("Mã sinh viên này đã được đăng kí, bạn quên mật khẩu ? Chúng tôi sẽ giúp bạn lấy lại mật khẩu của mình");
                        }
                        else
                        {
                            SqlCommand command = new SqlCommand("spSignup", connection);
                            command.CommandType = CommandType.StoredProcedure;
                            SqlParameter thisUsername = command.Parameters.Add("@username", SqlDbType.VarChar);
                            thisUsername.Value = txtbUsername.Text;
                            SqlParameter thisPassword = command.Parameters.Add("@password", SqlDbType.VarChar);
                            thisPassword.Value = txtbPassword.Text;
                            SqlParameter thisStudentID = command.Parameters.Add("@studentID", SqlDbType.VarChar);
                            thisStudentID.Value = txtbStudentID.Text;
                            command.ExecuteNonQuery();
                            connection.Close();
                            MessageBox.Show("Đăng kí thành công, hãy bắt đầu đăng nhập");
                            Close();
                        }
                    }
                }
            }
        }

        /* viet code ngu qua */
        private void btnLogin_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
