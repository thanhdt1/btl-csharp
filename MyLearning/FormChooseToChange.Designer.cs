﻿namespace MyLearning
{
    partial class FormChooseToChange
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnChangePass = new System.Windows.Forms.Button();
            this.btnChangerUserID = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnChangePass
            // 
            this.btnChangePass.Location = new System.Drawing.Point(100, 90);
            this.btnChangePass.Margin = new System.Windows.Forms.Padding(2);
            this.btnChangePass.Name = "btnChangePass";
            this.btnChangePass.Size = new System.Drawing.Size(83, 29);
            this.btnChangePass.TabIndex = 0;
            this.btnChangePass.Text = "Đổi Mật Khẩu";
            this.btnChangePass.UseVisualStyleBackColor = true;
            this.btnChangePass.Click += new System.EventHandler(this.btnChangePass_Click);
            // 
            // btnChangerUserID
            // 
            this.btnChangerUserID.Location = new System.Drawing.Point(218, 90);
            this.btnChangerUserID.Margin = new System.Windows.Forms.Padding(2);
            this.btnChangerUserID.Name = "btnChangerUserID";
            this.btnChangerUserID.Size = new System.Drawing.Size(83, 29);
            this.btnChangerUserID.TabIndex = 1;
            this.btnChangerUserID.Text = "Đổi Mã SV";
            this.btnChangerUserID.UseVisualStyleBackColor = true;
            this.btnChangerUserID.Click += new System.EventHandler(this.btnChangerUserID_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(71, 28);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(269, 31);
            this.label3.TabIndex = 5;
            this.label3.Text = "Đổi thông tin cá nhân";
            // 
            // FormChooseToChange
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(403, 173);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnChangerUserID);
            this.Controls.Add(this.btnChangePass);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "FormChooseToChange";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MyLearning - My Info";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnChangePass;
        private System.Windows.Forms.Button btnChangerUserID;
        private System.Windows.Forms.Label label3;
    }
}