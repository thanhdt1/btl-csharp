﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyLearning
{
    public partial class FormLogin : Form
    {
        string connectionString = ConfigurationManager.ConnectionStrings["Test"].ConnectionString;
        int userid;
        public FormLogin()
        {
            InitializeComponent();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            if (txtbUsername.Text == "" || txtbPassword.Text == "")
                MessageBox.Show("Yêu cầu nhập đủ thông tin đăng nhập");
            else
            {
                var connection = new SqlConnection(connectionString);
                SqlCommand command = new SqlCommand("spLoginCheck", connection);
                command.CommandType = CommandType.StoredProcedure;
                SqlParameter thisUsername = command.Parameters.Add("@username", SqlDbType.VarChar);
                thisUsername.Value = txtbUsername.Text;
                SqlParameter thisPassword = command.Parameters.Add("@password", SqlDbType.VarChar);
                thisPassword.Value = txtbPassword.Text;
                connection.Open();
                var dataReader = command.ExecuteReader(CommandBehavior.CloseConnection);
                if (dataReader.HasRows)
                {
                    while (dataReader.Read())
                    { userid = dataReader.GetInt32(0); }
                    FormHome formHome = new FormHome(txtbUsername.Text,userid);
                    Hide();
                    formHome.ShowDialog();
                    Show();
                }
                else
                    MessageBox.Show("Sai tên đăng nhập hoặc mật khẩu");
                connection.Close();
            }
            
        }

        private void btnSignup_Click(object sender, EventArgs e)
        {
            FormSignup formSignup = new FormSignup();
            Hide();
            formSignup.ShowDialog();
            Show();
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            FormForgotPW formForgotPW = new FormForgotPW();
            Hide();
            formForgotPW.ShowDialog();
            Show();
        }
    }
}
