﻿namespace MyLearning
{
    partial class FormChangePass
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnBack = new System.Windows.Forms.Button();
            this.btnChangePass = new System.Windows.Forms.Button();
            this.txtbNewPass = new System.Windows.Forms.TextBox();
            this.txtbOldPass = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtbNewPass2 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnBack
            // 
            this.btnBack.Location = new System.Drawing.Point(282, 163);
            this.btnBack.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(57, 30);
            this.btnBack.TabIndex = 5;
            this.btnBack.Text = "Quay Lại";
            this.btnBack.UseVisualStyleBackColor = true;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // btnChangePass
            // 
            this.btnChangePass.Location = new System.Drawing.Point(178, 163);
            this.btnChangePass.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnChangePass.Name = "btnChangePass";
            this.btnChangePass.Size = new System.Drawing.Size(56, 30);
            this.btnChangePass.TabIndex = 6;
            this.btnChangePass.Text = "Đổi MK";
            this.btnChangePass.UseVisualStyleBackColor = true;
            this.btnChangePass.Click += new System.EventHandler(this.BtnChangePass_Click);
            // 
            // txtbNewPass
            // 
            this.txtbNewPass.Location = new System.Drawing.Point(155, 83);
            this.txtbNewPass.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtbNewPass.MaxLength = 16;
            this.txtbNewPass.Name = "txtbNewPass";
            this.txtbNewPass.PasswordChar = '*';
            this.txtbNewPass.Size = new System.Drawing.Size(238, 20);
            this.txtbNewPass.TabIndex = 15;
            this.txtbNewPass.UseSystemPasswordChar = true;
            // 
            // txtbOldPass
            // 
            this.txtbOldPass.Location = new System.Drawing.Point(155, 42);
            this.txtbOldPass.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtbOldPass.MaxLength = 16;
            this.txtbOldPass.Name = "txtbOldPass";
            this.txtbOldPass.PasswordChar = '*';
            this.txtbOldPass.Size = new System.Drawing.Size(238, 20);
            this.txtbOldPass.TabIndex = 14;
            this.txtbOldPass.UseSystemPasswordChar = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(41, 124);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(99, 13);
            this.label5.TabIndex = 13;
            this.label5.Text = "Nhập Lại Mật Khẩu";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(41, 87);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(102, 13);
            this.label4.TabIndex = 12;
            this.label4.Text = "Nhập Mật Khẩu Mới";
            // 
            // txtbNewPass2
            // 
            this.txtbNewPass2.Location = new System.Drawing.Point(155, 120);
            this.txtbNewPass2.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtbNewPass2.MaxLength = 16;
            this.txtbNewPass2.Name = "txtbNewPass2";
            this.txtbNewPass2.PasswordChar = '*';
            this.txtbNewPass2.Size = new System.Drawing.Size(238, 20);
            this.txtbNewPass2.TabIndex = 17;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(41, 46);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(82, 13);
            this.label3.TabIndex = 16;
            this.label3.Text = "Nhập Mật Khẩu";
            // 
            // FormChangePass
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(469, 238);
            this.Controls.Add(this.txtbNewPass2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtbNewPass);
            this.Controls.Add(this.txtbOldPass);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btnChangePass);
            this.Controls.Add(this.btnBack);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "FormChangePass";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MyLearning - Change Password";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.Button btnChangePass;
        private System.Windows.Forms.TextBox txtbNewPass;
        private System.Windows.Forms.TextBox txtbOldPass;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtbNewPass2;
        private System.Windows.Forms.Label label3;
    }
}