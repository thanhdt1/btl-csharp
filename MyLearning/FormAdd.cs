﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyLearning
{
    public partial class FormAdd : Form
    {
        string connectionString = ConfigurationManager.ConnectionStrings["Test"].ConnectionString;
        int userid,subjectid,passSTT;
        double firstmark, secondmark, thirdmark, submark;
        String submarkbyalpha;
        List<int> subjectIdList;
        class DataItem
        {
            public int iID { get; set; }
            public string sName { get; set; }
        }
        public FormAdd(int _userid, String _username, List<int> _subjectIdList)
        {
            InitializeComponent();
            userid = _userid;
            subjectIdList = _subjectIdList;
        }
        private void FormAdd_Load(object sender, EventArgs e)
        {
            var connection = new SqlConnection(connectionString);
            SqlCommand command = new SqlCommand("spReturnSubjectName", connection);
            command.CommandType = CommandType.StoredProcedure;
            connection.Open();
            SqlDataReader dataReader = command.ExecuteReader();
            if (dataReader.HasRows)
            {
                while (dataReader.Read())
                {
                    cbSubject.Items.Add(new DataItem()
                    {
                        iID = dataReader.GetInt32(0),
                        sName = dataReader.GetString(1)
                    });
                }
                cbSubject.DisplayMember = "sName";
                cbSubject.ValueMember = "iID";
            }
            connection.Close();
        }
        private void numberInputOnly (object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
                MessageBox.Show("Chỉ được nhập số");
            }

            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
                MessageBox.Show("Yêu cầu nhập đúng định dạng số thực");
            }
        }

        private void cbSubject_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbSubject.SelectedIndex != -1)
            {
                DataItem selectedItem = cbSubject.SelectedItem as DataItem;
                if (selectedItem != null)
                    subjectid = selectedItem.iID;
                foreach (int Item in subjectIdList)
                {
                    if (subjectid == Item)
                    {
                        cbSubject.SelectedIndex = -1;
                        MessageBox.Show("Môn này đã được thêm từ trước, hãy chọn lại");
                        break;
                    }
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            var connection = new SqlConnection(connectionString);
            if (txtbFirstMark.Text == "" || txtbSecondMark.Text == "" || txtbThirdMark.Text == "" || cbSubject.SelectedIndex == -1)
                MessageBox.Show("Yêu cầu nhập đủ thông tin");
            else
            {
                firstmark = double.Parse(txtbFirstMark.Text);
                secondmark = double.Parse(txtbSecondMark.Text);
                thirdmark = double.Parse(txtbThirdMark.Text);
                if (firstmark < 0 || firstmark>10 || secondmark < 0 || secondmark > 10 || thirdmark < 0 || thirdmark > 10)
                {
                    MessageBox.Show("Điểm số chỉ được nằm trong đoạn từ 0 đến 10, vui lòng kiểm tra và nhập lại");
                }
                else
                {
                    double submarkbyten = (0.1 * firstmark) + (0.2 * secondmark) + (0.7 * thirdmark);
                    if (submarkbyten >= 9.45 && submarkbyten <= 10)
                    {
                        submarkbyalpha = "A+";
                        submark = 4;
                    }
                    if (submarkbyten < 9.45 && submarkbyten >= 8.45)
                    {
                        submarkbyalpha = "A";
                        submark = 4;
                    }
                    if (submarkbyten >= 7.95 && submarkbyten < 8.45)
                    {
                        submarkbyalpha = "B+";
                        submark = 3.5;
                    }
                    if (submarkbyten >= 6.95 && submarkbyten < 7.95)
                    {
                        submarkbyalpha = "B";
                        submark = 3;
                    }
                    if (submarkbyten >= 6.45 && submarkbyten < 6.95)
                    {
                        submarkbyalpha = "C+";
                        submark = 2.5;
                    }
                    if (submarkbyten >= 5.45 && submarkbyten < 6.45)
                    {
                        submarkbyalpha = "C";
                        submark = 2;
                    }
                    if (submarkbyten >= 4.95 && submarkbyten < 5.45)
                    {
                        submarkbyalpha = "D+";
                        submark = 1.5;
                    }
                    if (submarkbyten >= 4 && submarkbyten < 4.95)
                    {
                        submarkbyalpha = "D";
                        submark = 1;
                    }
                    if (submarkbyten < 4)
                    {
                        submarkbyalpha = "F";
                        submark = 0;
                    }
                    if (submark < 1)
                        passSTT = 0;
                    else
                        passSTT = 1;
                    var command = new SqlCommand("spAddMark", connection);
                    command.CommandType = CommandType.StoredProcedure;

                    SqlParameter dtSubjectID = command.Parameters.Add("@subjectID", SqlDbType.Int);
                    dtSubjectID.Value = subjectid;
                    SqlParameter dtUserID = command.Parameters.Add("@userID", SqlDbType.Int);
                    dtUserID.Value = userid;
                    SqlParameter dtFirstMark = command.Parameters.Add("@firstmark", SqlDbType.Real);
                    dtFirstMark.Value = firstmark;
                    SqlParameter dtSecondMark = command.Parameters.Add("@secondmark", SqlDbType.Real);
                    dtSecondMark.Value = secondmark;
                    SqlParameter dtThirdMark = command.Parameters.Add("@thirdmark", SqlDbType.Real);
                    dtThirdMark.Value = thirdmark;
                    SqlParameter dtSubMark = command.Parameters.Add("@submark", SqlDbType.Real);
                    dtSubMark.Value = submark;
                    SqlParameter dtSubMarkByAlpha = command.Parameters.Add("@submarkbyalpha", SqlDbType.VarChar);
                    dtSubMarkByAlpha.Value = submarkbyalpha.ToString();
                    SqlParameter dtPassSTT = command.Parameters.Add("@pass", SqlDbType.Bit);
                    dtPassSTT.Value = passSTT;
                    connection.Open();
                    command.ExecuteNonQuery();
                    connection.Close();

                    if (MessageBox.Show("Thêm thành công") == DialogResult.OK)
                    {
                        Close();
                    }
                }
            }
        }

        private void txtbFirstMark_KeyPress(object sender, KeyPressEventArgs e)
        {
            numberInputOnly(sender, e);
        }

        private void txtbSecondMark_KeyPress(object sender, KeyPressEventArgs e)
        {
            numberInputOnly(sender, e);
        }

        private void txtbThirdMark_KeyPress(object sender, KeyPressEventArgs e)
        {
            numberInputOnly(sender, e);
        }
    }
}
