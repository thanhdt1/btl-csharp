﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyLearning
{
    public partial class FormUpdate : Form
    {
        string connectionString = ConfigurationManager.ConnectionStrings["Test"].ConnectionString;
        int markid,passSTT;
        double firstmark, secondmark, thirdmark, submark;
        String submarkbyalpha;

        private void numberInputOnly(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
                MessageBox.Show("Chỉ được nhập số");
            }

            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
                MessageBox.Show("Yêu cầu nhập đúng định dạng số thực");
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {     
            var connection = new SqlConnection(connectionString);
            if (txtbFirstMark.Text == "" || txtbSecondMark.Text == "" || txtbThirdMark.Text == "")
                MessageBox.Show("Yêu cầu nhập đủ thông tin");
            else
            {
                firstmark = double.Parse(txtbFirstMark.Text);
                secondmark = double.Parse(txtbSecondMark.Text);
                thirdmark = double.Parse(txtbThirdMark.Text);
                if (firstmark < 0 || firstmark > 10 || secondmark < 0 || secondmark > 10 || thirdmark < 0 || thirdmark > 10)
                {
                    MessageBox.Show("Điểm số chỉ được nằm trong đoạn từ 0 đến 10, vui lòng kiểm tra và nhập lại");
                }
                else
                {
                    double submarkbyten = (0.1 * firstmark) + (0.2 * secondmark) + (0.7 * thirdmark);
                    if (submarkbyten >= 9.45 && submarkbyten <= 10)
                    {
                        submarkbyalpha = "A+";
                        submark = 4;
                    }
                    if (submarkbyten < 9.45 && submarkbyten >= 8.45)
                    {
                        submarkbyalpha = "A";
                        submark = 4;
                    }
                    if (submarkbyten >= 7.95 && submarkbyten < 8.45)
                    {
                        submarkbyalpha = "B+";
                        submark = 3.5;
                    }
                    if (submarkbyten >= 6.95 && submarkbyten < 7.95)
                    {
                        submarkbyalpha = "B";
                        submark = 3;
                    }
                    if (submarkbyten >= 6.45 && submarkbyten < 6.95)
                    {
                        submarkbyalpha = "C+";
                        submark = 2.5;
                    }
                    if (submarkbyten >= 5.45 && submarkbyten < 6.45)
                    {
                        submarkbyalpha = "C";
                        submark = 2;
                    }
                    if (submarkbyten >= 4.95 && submarkbyten < 5.45)
                    {
                        submarkbyalpha = "D+";
                        submark = 1.5;
                    }
                    if (submarkbyten >= 4 && submarkbyten < 4.95)
                    {
                        submarkbyalpha = "D";
                        submark = 1;
                    }
                    if (submarkbyten < 4)
                    {
                        submarkbyalpha = "F";
                        submark = 0;
                    }
                    if (submark < 1)
                        passSTT = 0;
                    else
                        passSTT = 1;
                    var command = new SqlCommand("spUpdateMark", connection);
                    command.CommandType = CommandType.StoredProcedure;
                    SqlParameter sqlMarkID = command.Parameters.Add("@markid", SqlDbType.Int);
                    sqlMarkID.Value = markid;
                    SqlParameter sqlFirstMark = command.Parameters.Add("@firstmark", SqlDbType.Real);
                    sqlFirstMark.Value = firstmark;
                    SqlParameter sqlSecondMark = command.Parameters.Add("@secondmark", SqlDbType.Real);
                    sqlSecondMark.Value = secondmark;
                    SqlParameter sqlThirdMark = command.Parameters.Add("@thirdmark", SqlDbType.Real);
                    sqlThirdMark.Value = thirdmark;
                    SqlParameter sqlSubMark = command.Parameters.Add("@submark", SqlDbType.Real);
                    sqlSubMark.Value = submark;
                    SqlParameter sqlSubMarkByAlpha = command.Parameters.Add("@submarkbyalpha", SqlDbType.VarChar);
                    sqlSubMarkByAlpha.Value = submarkbyalpha;
                    SqlParameter dtPassSTT = command.Parameters.Add("@pass", SqlDbType.Bit);
                    dtPassSTT.Value = passSTT;
                    connection.Open();
                    command.ExecuteNonQuery();
                    connection.Close();

                    if (MessageBox.Show("Sửa thành công") == DialogResult.OK)
                    {
                        Close();
                    }

                }
            }
        }

        private void txtbFirstMark_KeyPress(object sender, KeyPressEventArgs e)
        {
            numberInputOnly(sender, e);
        }

        private void txtbSecondMark_KeyPress(object sender, KeyPressEventArgs e)
        {
            numberInputOnly(sender, e);
        }

        private void txtbThirdMark_KeyPress(object sender, KeyPressEventArgs e)
        {
            numberInputOnly(sender, e);
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            var connection = new SqlConnection(connectionString);
            var command = new SqlCommand("spDeleteMark", connection);
            command.CommandType = CommandType.StoredProcedure;
            SqlParameter sqlMarkID = command.Parameters.Add("@markid", SqlDbType.Int);
            sqlMarkID.Value = markid;
            connection.Open();
            command.ExecuteNonQuery();
            connection.Close();
            if (MessageBox.Show("Đã xóa thành công") == DialogResult.OK)
            {
                Close();
            }
        }

        public FormUpdate(int _markid)
        {
            InitializeComponent();
            markid = _markid;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void FormUpdate_Load(object sender, EventArgs e)
        {
            var connection = new SqlConnection(connectionString);
            var command = new SqlCommand("spReturnMarkForUpdate", connection);
            command.CommandType = CommandType.StoredProcedure;
            SqlParameter sqlMarkID = command.Parameters.Add("@markid", SqlDbType.Int);
            sqlMarkID.Value = markid;
            connection.Open();
            var dataReader = command.ExecuteReader(CommandBehavior.CloseConnection);
            if (dataReader.HasRows)
            {
                while (dataReader.Read())
                { 
                    txtbSubjectName.Text = dataReader.GetString(0);
                }
            }
            connection.Close();
        }
    }
}
