﻿namespace MyLearning
{
    partial class FormResetPW
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtbNewPassword = new System.Windows.Forms.TextBox();
            this.txtbNewPasswordConfirm = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnChangePW = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtbNewPassword
            // 
            this.txtbNewPassword.Location = new System.Drawing.Point(136, 58);
            this.txtbNewPassword.MaxLength = 16;
            this.txtbNewPassword.Name = "txtbNewPassword";
            this.txtbNewPassword.PasswordChar = '*';
            this.txtbNewPassword.Size = new System.Drawing.Size(217, 20);
            this.txtbNewPassword.TabIndex = 0;
            this.txtbNewPassword.UseSystemPasswordChar = true;
            // 
            // txtbNewPasswordConfirm
            // 
            this.txtbNewPasswordConfirm.Location = new System.Drawing.Point(136, 84);
            this.txtbNewPasswordConfirm.MaxLength = 16;
            this.txtbNewPasswordConfirm.Name = "txtbNewPasswordConfirm";
            this.txtbNewPasswordConfirm.PasswordChar = '*';
            this.txtbNewPasswordConfirm.Size = new System.Drawing.Size(217, 20);
            this.txtbNewPasswordConfirm.TabIndex = 1;
            this.txtbNewPasswordConfirm.UseSystemPasswordChar = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(26, 61);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Mật khẩu mới";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(26, 87);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Nhập lại MK mới";
            // 
            // btnChangePW
            // 
            this.btnChangePW.Location = new System.Drawing.Point(149, 131);
            this.btnChangePW.Name = "btnChangePW";
            this.btnChangePW.Size = new System.Drawing.Size(109, 23);
            this.btnChangePW.TabIndex = 4;
            this.btnChangePW.Text = "Đổi mật khẩu";
            this.btnChangePW.UseVisualStyleBackColor = true;
            this.btnChangePW.Click += new System.EventHandler(this.btnchangePW_Click);
            // 
            // FormResetPW
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(388, 196);
            this.Controls.Add(this.btnChangePW);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtbNewPasswordConfirm);
            this.Controls.Add(this.txtbNewPassword);
            this.Name = "FormResetPW";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MyLearning - Reset Password";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtbNewPassword;
        private System.Windows.Forms.TextBox txtbNewPasswordConfirm;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnChangePW;
    }
}