﻿namespace MyLearning
{
    partial class FormSignup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lbUsername = new System.Windows.Forms.Label();
            this.labelPw = new System.Windows.Forms.Label();
            this.lbPwconfirm = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtbUsername = new System.Windows.Forms.TextBox();
            this.txtbPassword = new System.Windows.Forms.TextBox();
            this.txtbPasswordConfirm = new System.Windows.Forms.TextBox();
            this.txtbStudentID = new System.Windows.Forms.TextBox();
            this.btnSignup = new System.Windows.Forms.Button();
            this.btnLogin = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(176, 96);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(339, 16);
            this.label2.TabIndex = 3;
            this.label2.Text = "ỨNG DỤNG GIÚP BẠN THEO DÕI ĐIỂM SỐ CỦA MÌNH";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(248, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(208, 39);
            this.label1.TabIndex = 2;
            this.label1.Text = "MyLearning";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbUsername
            // 
            this.lbUsername.AutoSize = true;
            this.lbUsername.Location = new System.Drawing.Point(89, 180);
            this.lbUsername.Name = "lbUsername";
            this.lbUsername.Size = new System.Drawing.Size(81, 13);
            this.lbUsername.TabIndex = 4;
            this.lbUsername.Text = "Tên đăng nhập";
            // 
            // labelPw
            // 
            this.labelPw.AutoSize = true;
            this.labelPw.Location = new System.Drawing.Point(89, 206);
            this.labelPw.Name = "labelPw";
            this.labelPw.Size = new System.Drawing.Size(52, 13);
            this.labelPw.TabIndex = 5;
            this.labelPw.Text = "Mật khẩu";
            // 
            // lbPwconfirm
            // 
            this.lbPwconfirm.AutoSize = true;
            this.lbPwconfirm.Location = new System.Drawing.Point(89, 232);
            this.lbPwconfirm.Name = "lbPwconfirm";
            this.lbPwconfirm.Size = new System.Drawing.Size(93, 13);
            this.lbPwconfirm.TabIndex = 6;
            this.lbPwconfirm.Text = "Nhập lại mật khẩu";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(89, 258);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(67, 13);
            this.label6.TabIndex = 7;
            this.label6.Text = "Mã sinh viên";
            // 
            // txtbUsername
            // 
            this.txtbUsername.Location = new System.Drawing.Point(193, 177);
            this.txtbUsername.MaxLength = 50;
            this.txtbUsername.Name = "txtbUsername";
            this.txtbUsername.Size = new System.Drawing.Size(290, 20);
            this.txtbUsername.TabIndex = 8;
            // 
            // txtbPassword
            // 
            this.txtbPassword.Location = new System.Drawing.Point(193, 203);
            this.txtbPassword.MaxLength = 16;
            this.txtbPassword.Name = "txtbPassword";
            this.txtbPassword.PasswordChar = '*';
            this.txtbPassword.Size = new System.Drawing.Size(290, 20);
            this.txtbPassword.TabIndex = 9;
            this.txtbPassword.UseSystemPasswordChar = true;
            // 
            // txtbPasswordConfirm
            // 
            this.txtbPasswordConfirm.Location = new System.Drawing.Point(193, 229);
            this.txtbPasswordConfirm.MaxLength = 16;
            this.txtbPasswordConfirm.Name = "txtbPasswordConfirm";
            this.txtbPasswordConfirm.PasswordChar = '*';
            this.txtbPasswordConfirm.Size = new System.Drawing.Size(290, 20);
            this.txtbPasswordConfirm.TabIndex = 10;
            this.txtbPasswordConfirm.UseSystemPasswordChar = true;
            // 
            // txtbStudentID
            // 
            this.txtbStudentID.Location = new System.Drawing.Point(193, 255);
            this.txtbStudentID.MaxLength = 12;
            this.txtbStudentID.Name = "txtbStudentID";
            this.txtbStudentID.Size = new System.Drawing.Size(290, 20);
            this.txtbStudentID.TabIndex = 11;
            // 
            // btnSignup
            // 
            this.btnSignup.Location = new System.Drawing.Point(193, 301);
            this.btnSignup.Name = "btnSignup";
            this.btnSignup.Size = new System.Drawing.Size(106, 23);
            this.btnSignup.TabIndex = 12;
            this.btnSignup.Text = "Đăng ký";
            this.btnSignup.UseVisualStyleBackColor = true;
            this.btnSignup.Click += new System.EventHandler(this.btnSignup_Click);
            // 
            // btnLogin
            // 
            this.btnLogin.Location = new System.Drawing.Point(319, 301);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Size = new System.Drawing.Size(164, 23);
            this.btnLogin.TabIndex = 13;
            this.btnLogin.Text = "Đã có TK? Đăng nhập ngay";
            this.btnLogin.UseVisualStyleBackColor = true;
            this.btnLogin.Click += new System.EventHandler(this.btnLogin_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(501, 180);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(99, 13);
            this.label3.TabIndex = 14;
            this.label3.Text = "Không quá 50 kí tự";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(501, 206);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(99, 13);
            this.label4.TabIndex = 15;
            this.label4.Text = "Không quá 16 kí tự";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.Color.Red;
            this.label5.Location = new System.Drawing.Point(501, 232);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(99, 13);
            this.label5.TabIndex = 16;
            this.label5.Text = "Không quá 16 kí tự";
            // 
            // FormSignup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(673, 391);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnLogin);
            this.Controls.Add(this.btnSignup);
            this.Controls.Add(this.txtbStudentID);
            this.Controls.Add(this.txtbPasswordConfirm);
            this.Controls.Add(this.txtbPassword);
            this.Controls.Add(this.txtbUsername);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.lbPwconfirm);
            this.Controls.Add(this.labelPw);
            this.Controls.Add(this.lbUsername);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "FormSignup";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FormSignup";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lbUsername;
        private System.Windows.Forms.Label labelPw;
        private System.Windows.Forms.Label lbPwconfirm;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtbUsername;
        private System.Windows.Forms.TextBox txtbPassword;
        private System.Windows.Forms.TextBox txtbPasswordConfirm;
        private System.Windows.Forms.TextBox txtbStudentID;
        private System.Windows.Forms.Button btnSignup;
        private System.Windows.Forms.Button btnLogin;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
    }
}