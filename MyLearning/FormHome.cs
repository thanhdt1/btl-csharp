﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;
using CrystalDecisions.CrystalReports.Engine;

namespace MyLearning
{
    public partial class FormHome : Form
    {
        string connectionString = ConfigurationManager.ConnectionStrings["Test"].ConnectionString;
        String username;
        int userid;
        List<int> subjectIdList = new List<int>();
        public FormHome(String _username, int _userid)
        {
            InitializeComponent();
            username = _username;
            userid = _userid;
        }

        private void FormHome_Load(object sender, EventArgs e)
        {
            subjectIdList.Clear();
            lbWelcome.Text = "Xin chào " + username;
            var connection = new SqlConnection(connectionString);
            SqlCommand command = new SqlCommand("spReturnMark", connection);
            command.CommandType = CommandType.StoredProcedure;
            SqlParameter userID = command.Parameters.Add("@userid", SqlDbType.Int);
            userID.Value = userid;
            connection.Open();
            SqlDataAdapter da = new SqlDataAdapter(command);
            DataTable dt = new DataTable();
            da.Fill(dt);
            var dataReader = command.ExecuteReader(CommandBehavior.CloseConnection);
            if (dataReader.HasRows)
            {
                while (dataReader.Read())
                { subjectIdList.Add(dataReader.GetInt32(1)); }
            }
            connection.Close();
            dtgrMarks.DataSource = dt;
        }

        private void lbSignout_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Close();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            FormAdd formAdd = new FormAdd(userid, username, subjectIdList);
            formAdd.ShowDialog();
            FormHome_Load(sender, e);
        }

        private void dtgrMarks_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                int markid = int.Parse(dtgrMarks.Rows[e.RowIndex].Cells[0].Value.ToString());
                FormUpdate formUpdate = new FormUpdate(markid);
                formUpdate.ShowDialog();
                FormHome_Load(sender, e);
                //int rowCount = dtgrMarks.BindingContext[dtgrMarks.DataSource].Count;
                //if (rowCount < e.RowIndex)
                //{
                //    dtgrMarks.CurrentCell.Selected = true;
                //    //dtgrMarks.Rows[e.RowIndex].Cells[2].Selected = true;
                //}
            }
        }

        private void btnReport_Click(object sender, EventArgs e)
        {
            //FormReport formReport = new FormReport();
            //formReport.ShowDialog();
        }//Đoạn này sẽ sửa để lên 1 form hiện trạng thái học tập (xếp loại, TBC tích lũy), trong form đó sẽ có 1 nút xuất báo cáo sau

        private void llbUsername_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            FormChooseToChange FormChooseToChange = new FormChooseToChange(userid, username);
            Hide();
            FormChooseToChange.ShowDialog();
            Show();
        }
    }
}
