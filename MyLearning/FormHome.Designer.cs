﻿namespace MyLearning
{
    partial class FormHome
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lbWelcome = new System.Windows.Forms.Label();
            this.lbSignout = new System.Windows.Forms.LinkLabel();
            this.dtgrMarks = new System.Windows.Forms.DataGridView();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnReport = new System.Windows.Forms.Button();
            this.llbChangeInfo = new System.Windows.Forms.LinkLabel();
            this.sName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iCredit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rFirstMark = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rSecondMark = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rThirdMark = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rSubMark = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sSubMarkByAlpha = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bPassStatus = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.iSubjectID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dtgrMarks)).BeginInit();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(55, 67);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(339, 16);
            this.label2.TabIndex = 3;
            this.label2.Text = "ỨNG DỤNG GIÚP BẠN THEO DÕI ĐIỂM SỐ CỦA MÌNH";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(51, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(208, 39);
            this.label1.TabIndex = 2;
            this.label1.Text = "MyLearning";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbWelcome
            // 
            this.lbWelcome.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbWelcome.AutoSize = true;
            this.lbWelcome.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbWelcome.Location = new System.Drawing.Point(971, 28);
            this.lbWelcome.Name = "lbWelcome";
            this.lbWelcome.Size = new System.Drawing.Size(98, 13);
            this.lbWelcome.TabIndex = 5;
            this.lbWelcome.Text = "Xin chào username";
            this.lbWelcome.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbSignout
            // 
            this.lbSignout.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbSignout.AutoSize = true;
            this.lbSignout.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbSignout.Location = new System.Drawing.Point(1085, 57);
            this.lbSignout.Name = "lbSignout";
            this.lbSignout.Size = new System.Drawing.Size(56, 13);
            this.lbSignout.TabIndex = 6;
            this.lbSignout.TabStop = true;
            this.lbSignout.Text = "Đăng xuất";
            this.lbSignout.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lbSignout_LinkClicked);
            // 
            // dtgrMarks
            // 
            this.dtgrMarks.AllowUserToAddRows = false;
            this.dtgrMarks.AllowUserToDeleteRows = false;
            this.dtgrMarks.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtgrMarks.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dtgrMarks.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dtgrMarks.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgrMarks.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.sName,
            this.iCredit,
            this.rFirstMark,
            this.rSecondMark,
            this.rThirdMark,
            this.rSubMark,
            this.sSubMarkByAlpha,
            this.bPassStatus,
            this.iSubjectID,
            this.iID});
            this.dtgrMarks.Location = new System.Drawing.Point(67, 127);
            this.dtgrMarks.Name = "dtgrMarks";
            this.dtgrMarks.ReadOnly = true;
            this.dtgrMarks.RowHeadersWidth = 51;
            this.dtgrMarks.Size = new System.Drawing.Size(1074, 428);
            this.dtgrMarks.TabIndex = 4;
            this.dtgrMarks.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dtgrMarks_CellClick);
            // 
            // btnAdd
            // 
            this.btnAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAdd.Location = new System.Drawing.Point(956, 575);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(75, 23);
            this.btnAdd.TabIndex = 7;
            this.btnAdd.Text = "Thêm";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnReport
            // 
            this.btnReport.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnReport.Location = new System.Drawing.Point(1049, 575);
            this.btnReport.Name = "btnReport";
            this.btnReport.Size = new System.Drawing.Size(92, 23);
            this.btnReport.TabIndex = 8;
            this.btnReport.Text = "Xuất kết quả";
            this.btnReport.UseVisualStyleBackColor = true;
            this.btnReport.Click += new System.EventHandler(this.btnReport_Click);
            // 
            // llbChangeInfo
            // 
            this.llbChangeInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.llbChangeInfo.AutoSize = true;
            this.llbChangeInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.llbChangeInfo.Location = new System.Drawing.Point(971, 57);
            this.llbChangeInfo.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.llbChangeInfo.Name = "llbChangeInfo";
            this.llbChangeInfo.Size = new System.Drawing.Size(109, 13);
            this.llbChangeInfo.TabIndex = 9;
            this.llbChangeInfo.TabStop = true;
            this.llbChangeInfo.Text = "Đổi thông tin cá nhân";
            this.llbChangeInfo.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.llbUsername_LinkClicked);
            // 
            // sName
            // 
            this.sName.DataPropertyName = "sName";
            this.sName.HeaderText = "Tên môn học";
            this.sName.MinimumWidth = 6;
            this.sName.Name = "sName";
            this.sName.ReadOnly = true;
            this.sName.Width = 200;
            // 
            // iCredit
            // 
            this.iCredit.DataPropertyName = "iCredit";
            this.iCredit.HeaderText = "Số tín chỉ";
            this.iCredit.MinimumWidth = 6;
            this.iCredit.Name = "iCredit";
            this.iCredit.ReadOnly = true;
            this.iCredit.Width = 80;
            // 
            // rFirstMark
            // 
            this.rFirstMark.DataPropertyName = "rFirstMark";
            this.rFirstMark.HeaderText = "Điểm chuyên cần";
            this.rFirstMark.MinimumWidth = 6;
            this.rFirstMark.Name = "rFirstMark";
            this.rFirstMark.ReadOnly = true;
            this.rFirstMark.Width = 120;
            // 
            // rSecondMark
            // 
            this.rSecondMark.DataPropertyName = "rSecondMark";
            this.rSecondMark.HeaderText = "Điểm giữa kì";
            this.rSecondMark.MinimumWidth = 6;
            this.rSecondMark.Name = "rSecondMark";
            this.rSecondMark.ReadOnly = true;
            this.rSecondMark.Width = 120;
            // 
            // rThirdMark
            // 
            this.rThirdMark.DataPropertyName = "rThirdMark";
            this.rThirdMark.HeaderText = "Điểm thi";
            this.rThirdMark.MinimumWidth = 6;
            this.rThirdMark.Name = "rThirdMark";
            this.rThirdMark.ReadOnly = true;
            this.rThirdMark.Width = 120;
            // 
            // rSubMark
            // 
            this.rSubMark.DataPropertyName = "rSubMark";
            this.rSubMark.HeaderText = "Điểm theo thang 4";
            this.rSubMark.MinimumWidth = 6;
            this.rSubMark.Name = "rSubMark";
            this.rSubMark.ReadOnly = true;
            this.rSubMark.Width = 150;
            // 
            // sSubMarkByAlpha
            // 
            this.sSubMarkByAlpha.DataPropertyName = "sSubMarkByAlpha";
            this.sSubMarkByAlpha.HeaderText = "Điểm thang 4 theo chữ";
            this.sSubMarkByAlpha.MinimumWidth = 6;
            this.sSubMarkByAlpha.Name = "sSubMarkByAlpha";
            this.sSubMarkByAlpha.ReadOnly = true;
            this.sSubMarkByAlpha.Width = 140;
            // 
            // bPassStatus
            // 
            this.bPassStatus.DataPropertyName = "bPassStatus";
            this.bPassStatus.HeaderText = "Qua môn";
            this.bPassStatus.MinimumWidth = 6;
            this.bPassStatus.Name = "bPassStatus";
            this.bPassStatus.ReadOnly = true;
            this.bPassStatus.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.bPassStatus.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.bPassStatus.Width = 90;
            // 
            // iSubjectID
            // 
            this.iSubjectID.DataPropertyName = "iSubjectID";
            this.iSubjectID.HeaderText = "iSubjectID";
            this.iSubjectID.MinimumWidth = 6;
            this.iSubjectID.Name = "iSubjectID";
            this.iSubjectID.ReadOnly = true;
            this.iSubjectID.Visible = false;
            this.iSubjectID.Width = 125;
            // 
            // iID
            // 
            this.iID.DataPropertyName = "iID";
            this.iID.HeaderText = "iID";
            this.iID.MinimumWidth = 6;
            this.iID.Name = "iID";
            this.iID.ReadOnly = true;
            this.iID.Visible = false;
            this.iID.Width = 125;
            // 
            // FormHome
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1211, 635);
            this.Controls.Add(this.llbChangeInfo);
            this.Controls.Add(this.btnReport);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.lbSignout);
            this.Controls.Add(this.lbWelcome);
            this.Controls.Add(this.dtgrMarks);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "FormHome";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MyLearning - Home";
            this.Load += new System.EventHandler(this.FormHome_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dtgrMarks)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lbWelcome;
        private System.Windows.Forms.LinkLabel lbSignout;
        private System.Windows.Forms.DataGridView dtgrMarks;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnReport;
        private System.Windows.Forms.LinkLabel llbChangeInfo;
        private System.Windows.Forms.DataGridViewTextBoxColumn sName;
        private System.Windows.Forms.DataGridViewTextBoxColumn iCredit;
        private System.Windows.Forms.DataGridViewTextBoxColumn rFirstMark;
        private System.Windows.Forms.DataGridViewTextBoxColumn rSecondMark;
        private System.Windows.Forms.DataGridViewTextBoxColumn rThirdMark;
        private System.Windows.Forms.DataGridViewTextBoxColumn rSubMark;
        private System.Windows.Forms.DataGridViewTextBoxColumn sSubMarkByAlpha;
        private System.Windows.Forms.DataGridViewCheckBoxColumn bPassStatus;
        private System.Windows.Forms.DataGridViewTextBoxColumn iSubjectID;
        private System.Windows.Forms.DataGridViewTextBoxColumn iID;
    }
}