﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyLearning
{
    public partial class FormChangeStudentID : Form
    {
        int userid;
        String username;
        public FormChangeStudentID(int _userid, String _username)
        {
            InitializeComponent();
            this.userid = _userid;
            this.username = _username;
        }

        private void BtnChangeUserID_Click(object sender, EventArgs e)
        {
            if (txtbPass.Text == "" || txtbStudentID.Text == "")
                MessageBox.Show("Yêu cầu nhập đủ thông tin phần thông tin");
            else
            {
                var connectionString = @"Data Source=.\sqlexpress;Initial Catalog=dbMyLearning;Integrated Security=True";
                var connection = new SqlConnection(connectionString);
                SqlCommand spCheckPassword = new SqlCommand("spCheckPassword", connection);
                spCheckPassword.CommandType = CommandType.StoredProcedure;
                SqlParameter CheckPassword = spCheckPassword.Parameters.Add("@sPassword", SqlDbType.VarChar);
                CheckPassword.Value = txtbPass.Text;
                SqlParameter CheckID = spCheckPassword.Parameters.Add("@iID", SqlDbType.Int);
                CheckID.Value = userid;
                connection.Open();
                var dataReader = spCheckPassword.ExecuteReader(CommandBehavior.CloseConnection);
                if (dataReader.HasRows)
                {
                    SqlCommand spCheckStudentID = new SqlCommand("spCheckStudentID", connection);
                    spCheckStudentID.CommandType = CommandType.StoredProcedure;
                    SqlParameter CheckStudentID = spCheckStudentID.Parameters.Add("@sStudentID", SqlDbType.VarChar);
                    CheckStudentID.Value = txtbStudentID.Text;
                    connection.Close();
                    connection.Open();
                    var dataReaderID = spCheckStudentID.ExecuteReader(CommandBehavior.CloseConnection);
                    if (dataReaderID.HasRows)
                    {
                        MessageBox.Show("Mã sinh viên đã tồn tại yêu cầu nhập mã sinh viên khác");
                        connection.Close();

                    }
                    else
                    {
                        SqlCommand spChangeStudentID = new SqlCommand("spChangeStudentID", connection);
                        spChangeStudentID.CommandType = CommandType.StoredProcedure;
                        SqlParameter checkID = spChangeStudentID.Parameters.Add("@iID", SqlDbType.VarChar);
                        checkID.Value = userid;
                        SqlParameter NewStudentID = spChangeStudentID.Parameters.Add("@sStudentID", SqlDbType.VarChar);
                        NewStudentID.Value = txtbStudentID.Text;
                        connection.Close();
                        connection.Open();
                        spChangeStudentID.ExecuteNonQuery();
                        connection.Close();
                        MessageBox.Show("Thông tin đã được đổi");
                    }

                }
                else
                    MessageBox.Show("Nhập sai mật khẩu vui lòng nhập lại");
                connection.Close();
            }
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
