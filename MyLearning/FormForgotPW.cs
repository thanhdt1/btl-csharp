﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyLearning
{
    public partial class FormForgotPW : Form
    {
        string connectionString = ConfigurationManager.ConnectionStrings["Test"].ConnectionString;
        String username;
        public FormForgotPW()
        {
            InitializeComponent();
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            username = txtbUsername.Text;
            var connection = new SqlConnection(connectionString);
            SqlCommand command = new SqlCommand("spCheckForPWReset", connection);
            command.CommandType = CommandType.StoredProcedure;
            SqlParameter sqlUsername = command.Parameters.Add("@username", SqlDbType.VarChar);
            sqlUsername.Value = txtbUsername.Text;
            SqlParameter sqlStudentID = command.Parameters.Add("@studentid", SqlDbType.VarChar);
            sqlStudentID.Value = txtbStudentID.Text;
            connection.Open();
            var dataReader = command.ExecuteReader(CommandBehavior.CloseConnection);
            if (dataReader.HasRows)
            {
                FormResetPW formResetPW = new FormResetPW(username);
                Hide();
                formResetPW.ShowDialog();
                Close();
            }
            else
                MessageBox.Show("Tên đăng nhập và mã sinh viên của bạn vừa nhập vào không khớp với trên hệ thống, hãy kiểm tra lại");
            connection.Close();
        }
    }
}
