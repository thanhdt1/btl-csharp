﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyLearning
{
    public partial class FormResetPW : Form
    {
        string connectionString = ConfigurationManager.ConnectionStrings["Test"].ConnectionString;
        String username;
        public FormResetPW(String _username)
        {
            InitializeComponent();
            username = _username;
        }

        private void btnchangePW_Click(object sender, EventArgs e)
        {
            if (txtbNewPassword.Text == "" || txtbNewPasswordConfirm.Text == "")
                MessageBox.Show("Yêu cầu nhập đủ thông tin");
            else
            {
                {
                    if (txtbNewPassword.Text != txtbNewPasswordConfirm.Text)
                        MessageBox.Show("Mật khẩu nhập lại phải trùng khớp với mật khẩu trên");
                    else
                    {
                        var connection = new SqlConnection(connectionString);
                        SqlCommand ChangePW = new SqlCommand("spChangePW", connection);
                        ChangePW.CommandType = CommandType.StoredProcedure;
                        SqlParameter sqlUsername = ChangePW.Parameters.Add("@username", SqlDbType.VarChar);
                        sqlUsername.Value = username;
                        SqlParameter newPW = ChangePW.Parameters.Add("@newPW", SqlDbType.VarChar);
                        newPW.Value = txtbNewPassword.Text;
                        connection.Open();
                        ChangePW.ExecuteNonQuery();
                        connection.Close();
                        MessageBox.Show("Đổi mật khẩu thành công!");
                        Close();
                    }

                }
            }
        }
    }
}
