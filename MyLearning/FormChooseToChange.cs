﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyLearning
{
    public partial class FormChooseToChange : Form
    {
        int userid;
        String username;
        public FormChooseToChange(int _userid, string _username)
        {
            InitializeComponent();
            this.userid = _userid;
            this.username = _username;
        }

        private void btnChangePass_Click(object sender, EventArgs e)
        {
            FormChangePass FormChangePass = new FormChangePass(userid, username);
            Hide();
            FormChangePass.ShowDialog();
            Show();
        }

        private void FormChooseToChange_Load(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }//đoạn này nghĩa là gì ?

        private void btnChangerUserID_Click(object sender, EventArgs e)
        {
            FormChangeStudentID FormChangeUserID = new FormChangeStudentID(userid, username);
            Hide();
            FormChangeUserID.ShowDialog();
            Show();
        }


    }
}
