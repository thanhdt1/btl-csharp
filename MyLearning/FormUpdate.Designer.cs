﻿namespace MyLearning
{
    partial class FormUpdate
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtbSubjectName = new System.Windows.Forms.TextBox();
            this.txtbFirstMark = new System.Windows.Forms.TextBox();
            this.txtbSecondMark = new System.Windows.Forms.TextBox();
            this.txtbThirdMark = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(107, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(339, 16);
            this.label2.TabIndex = 5;
            this.label2.Text = "ỨNG DỤNG GIÚP BẠN THEO DÕI ĐIỂM SỐ CỦA MÌNH";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(173, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(208, 39);
            this.label1.TabIndex = 4;
            this.label1.Text = "MyLearning";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnUpdate
            // 
            this.btnUpdate.Location = new System.Drawing.Point(216, 274);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(86, 23);
            this.btnUpdate.TabIndex = 6;
            this.btnUpdate.Text = "Xác nhận sửa";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(308, 274);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(88, 23);
            this.btnDelete.TabIndex = 7;
            this.btnDelete.Text = "Xóa mục này";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(402, 274);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(112, 23);
            this.button3.TabIndex = 8;
            this.button3.Text = "Quay lại bảng điểm";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(53, 139);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(70, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Tên môn học";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(53, 165);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(90, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Điểm chuyên cần";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(53, 191);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(65, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "Điểm giữa kì";
            // 
            // txtbSubjectName
            // 
            this.txtbSubjectName.Location = new System.Drawing.Point(149, 136);
            this.txtbSubjectName.Name = "txtbSubjectName";
            this.txtbSubjectName.ReadOnly = true;
            this.txtbSubjectName.Size = new System.Drawing.Size(331, 20);
            this.txtbSubjectName.TabIndex = 12;
            // 
            // txtbFirstMark
            // 
            this.txtbFirstMark.Location = new System.Drawing.Point(149, 162);
            this.txtbFirstMark.Name = "txtbFirstMark";
            this.txtbFirstMark.Size = new System.Drawing.Size(331, 20);
            this.txtbFirstMark.TabIndex = 13;
            this.txtbFirstMark.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtbFirstMark_KeyPress);
            // 
            // txtbSecondMark
            // 
            this.txtbSecondMark.Location = new System.Drawing.Point(149, 188);
            this.txtbSecondMark.Name = "txtbSecondMark";
            this.txtbSecondMark.Size = new System.Drawing.Size(331, 20);
            this.txtbSecondMark.TabIndex = 14;
            this.txtbSecondMark.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtbSecondMark_KeyPress);
            // 
            // txtbThirdMark
            // 
            this.txtbThirdMark.Location = new System.Drawing.Point(149, 214);
            this.txtbThirdMark.Name = "txtbThirdMark";
            this.txtbThirdMark.Size = new System.Drawing.Size(331, 20);
            this.txtbThirdMark.TabIndex = 15;
            this.txtbThirdMark.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtbThirdMark_KeyPress);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(53, 217);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(45, 13);
            this.label6.TabIndex = 16;
            this.label6.Text = "Điểm thi";
            // 
            // FormUpdate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(536, 349);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtbThirdMark);
            this.Controls.Add(this.txtbSecondMark);
            this.Controls.Add(this.txtbFirstMark);
            this.Controls.Add(this.txtbSubjectName);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "FormUpdate";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MyLearning - Mark Edit";
            this.Load += new System.EventHandler(this.FormUpdate_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtbSubjectName;
        private System.Windows.Forms.TextBox txtbFirstMark;
        private System.Windows.Forms.TextBox txtbSecondMark;
        private System.Windows.Forms.TextBox txtbThirdMark;
        private System.Windows.Forms.Label label6;
    }
}