﻿namespace MyLearning
{
    partial class FormChangeStudentID
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnBack = new System.Windows.Forms.Button();
            this.btnChangeUserID = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtbPass = new System.Windows.Forms.TextBox();
            this.txtbStudentID = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // btnBack
            // 
            this.btnBack.Location = new System.Drawing.Point(258, 127);
            this.btnBack.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(72, 30);
            this.btnBack.TabIndex = 5;
            this.btnBack.Text = "Quay Lại";
            this.btnBack.UseVisualStyleBackColor = true;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // btnChangeUserID
            // 
            this.btnChangeUserID.Location = new System.Drawing.Point(152, 127);
            this.btnChangeUserID.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnChangeUserID.Name = "btnChangeUserID";
            this.btnChangeUserID.Size = new System.Drawing.Size(72, 30);
            this.btnChangeUserID.TabIndex = 6;
            this.btnChangeUserID.Text = "Đổi Mã SV";
            this.btnChangeUserID.UseVisualStyleBackColor = true;
            this.btnChangeUserID.Click += new System.EventHandler(this.BtnChangeUserID_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(31, 45);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(119, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Nhập Mã Sinh Viên Mới";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(31, 87);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(82, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Nhập Mật Khẩu";
            // 
            // txtbPass
            // 
            this.txtbPass.Location = new System.Drawing.Point(152, 87);
            this.txtbPass.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtbPass.MaxLength = 16;
            this.txtbPass.Name = "txtbPass";
            this.txtbPass.PasswordChar = '*';
            this.txtbPass.Size = new System.Drawing.Size(246, 20);
            this.txtbPass.TabIndex = 10;
            // 
            // txtbStudentID
            // 
            this.txtbStudentID.Location = new System.Drawing.Point(152, 42);
            this.txtbStudentID.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtbStudentID.Name = "txtbStudentID";
            this.txtbStudentID.Size = new System.Drawing.Size(246, 20);
            this.txtbStudentID.TabIndex = 11;
            // 
            // FormChangeStudentID
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(443, 196);
            this.Controls.Add(this.txtbStudentID);
            this.Controls.Add(this.txtbPass);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btnChangeUserID);
            this.Controls.Add(this.btnBack);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "FormChangeStudentID";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MyLearning - Change StudentID";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.Button btnChangeUserID;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtbPass;
        private System.Windows.Forms.TextBox txtbStudentID;
    }
}