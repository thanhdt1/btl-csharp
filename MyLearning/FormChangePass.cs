﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyLearning
{
    public partial class FormChangePass : Form
    {
        int userid;
        String username;
        string connectionString = ConfigurationManager.ConnectionStrings["Test"].ConnectionString;
        public FormChangePass(int _userid, String _username)
        {
            InitializeComponent();
            this.userid = _userid;
            this.username = _username;
        }

        private void BtnChangePass_Click(object sender, EventArgs e)
        {
            if (txtbOldPass.Text == "" || txtbNewPass.Text == "" || txtbNewPass2.Text == "")
                MessageBox.Show("Yêu cầu nhập đủ thông tin phần thông tin");       
            else
            {
                var connection = new SqlConnection(connectionString);
                SqlCommand command = new SqlCommand("spCheckPassword", connection);
                command.CommandType = CommandType.StoredProcedure;
                SqlParameter sqlStudentID = command.Parameters.Add("@sPassword", SqlDbType.VarChar);
                sqlStudentID.Value = txtbOldPass.Text;
                SqlParameter CheckID = command.Parameters.Add("@iID", SqlDbType.Int);
                CheckID.Value = userid;
                connection.Open();
                var dataReader = command.ExecuteReader(CommandBehavior.CloseConnection);
                if (dataReader.HasRows)
                {
                    if (txtbOldPass.Text == txtbNewPass.Text)
                        MessageBox.Show("Mật khẩu mới và cũ không được trùng nhau vùi lòng nhập lại");
                    else
                    if (txtbNewPass.Text != txtbNewPass2.Text)
                        MessageBox.Show("Mật khẩu không trùng nhau");
                    else
                    {
                        SqlCommand spChangeUserPassword = new SqlCommand("spChangeUserPassword", connection);
                        spChangeUserPassword.CommandType = CommandType.StoredProcedure;
                        SqlParameter CheckID1 = spChangeUserPassword.Parameters.Add("@iID", SqlDbType.VarChar);
                        CheckID1.Value = userid;
                        SqlParameter NewPassword = spChangeUserPassword.Parameters.Add("@sPassword", SqlDbType.VarChar);
                        NewPassword.Value = txtbNewPass.Text;
                        connection.Close();
                        connection.Open();
                        spChangeUserPassword.ExecuteNonQuery();
                        connection.Close();
                        MessageBox.Show("Mật khẩu đã được đổi");
                    }
                }
                else

                    MessageBox.Show("Nhập sai mật khẩu cũ vui lòng kiểm tra lại");
                connection.Close();

            }
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            Close();
        }

        
    }
}
